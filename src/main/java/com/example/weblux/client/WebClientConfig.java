package com.example.weblux.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Configuration
@Slf4j
public class WebClientConfig {
	/**
	 * reactive, non-blocking solution.
	 * Webflux is all about reactive programming, which in summary means that business logic is only executed as soon as the
	 * data to process it is available
	 * (reactive).
	 *
	 * This means you no longer can return simple POJO's, but you have to return something else,
	 * something that can provide the result when it's available.
	 */

	@Bean
	WebClient webClient() {
		return WebClient.builder()
				.filter(logRequest())
				.filter(logResponse())
				.build();
	}

	private static ExchangeFilterFunction logRequest() {
		return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
			log.info("Request: {} {}", clientRequest.method(), clientRequest.url());
			clientRequest.headers().forEach((name, values) -> values.forEach(value -> log.info("{}={}", name, value)));
			return Mono.just(clientRequest);
		});
	}

	//TODO: logging body
	private static ExchangeFilterFunction logResponse() {
		return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
			log.info("Response status: {}", clientResponse.statusCode());
			log.info("Response body: {}", clientResponse.bodyToMono(String.class));
			//clientResponse.headers().asHttpHeaders().forEach((name, values) -> values.forEach(value -> log.info("Response header {}={}", name, value)));
			return Mono.just(clientResponse);
		});
	}
}
