package com.example.weblux.client;

import reactor.core.scheduler.Schedulers;

public class ClientExecutor {
    /**
     *  Mostra la forza delle declarative API
     */
    public static void main(String args[]) {
        WebClientApi api = new WebClientApi();
        //salvo il prodotto
        api.postNewProduct()
                //thenMany aspetta il completamento del precedente
                .thenMany(api.getAllProducts())
                //prendo un elmento
                .take(1)
                //lo aggiorno
                .flatMap(p -> api.updateProduct(p.getId(), "White Tea", 0.99))
                //lo cancello
                .flatMap(p -> api.deleteProduct(p.getId()))
                //riprendo tutti i prodotti (non ce ne sarà nessuno)
                .thenMany(api.getAllProducts())
                //prendo gli eventi
                //.thenMany(api.getAllEvents())
                //richieste eseguite in un altro thread (dato che di default andrebbe nel main thread)
                .subscribeOn(Schedulers.newSingle("myThread"))
                //trigger di tutta la sequenza di operazioni (senza subscribe non si ottengono eventi)
                .subscribe(System.out::println);


        //Alternativa: stoppa il main thread e da il tempo di ricevere la response
//        try {
//            Thread.sleep(5000);
//        } catch(Exception e) { }
    }
}
