package com.example.weblux.client;

import com.example.weblux.model.Product;
import com.example.weblux.model.ProductEvent;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class WebClientApi {
    private WebClient webClient;

    /**
     * WebClient creato nel costruttore e con base url, ma che può essere anche creato come bean
     */
    public WebClientApi() {
        //this.webClient = WebClient.create("http://localhost:8080/products");
        this.webClient = WebClient.builder()
                .baseUrl("http://localhost:8080/products")
                .build();
    }

    public ResponseEntity blockExample() {
        return webClient.get()
                .uri("https://catfact.ninja/fact")
                .retrieve()
                .bodyToMono(ResponseEntity.class)
                .block();
    }

    public Mono<ResponseEntity<Product>> postNewProduct() {
        return webClient
                .post()
                //create new product
                .body(Mono.just(new Product(null, "Jasmine Tea", 1.99)), Product.class)
                //perform the request and get the body
                .exchangeToMono(response -> response.toEntity(Product.class))
                //onSuccess -->print. No subscribe yet, so no Product but only publisher
                .doOnSuccess(o -> System.out.println("**********POST " + o));
    }

    public Flux<Product> getAllProducts() {
        return webClient
                //get request to the base url
                .get()
                //retrieve singolo + bodyToFlux in alternativa a exchangeToFlux
                .retrieve()
                //flux perchè più di uno
                .bodyToFlux(Product.class)
                .doOnNext(o -> System.out.println("**********GET: " + o));
    }

    public Mono<Product> updateProduct(String id, String name, double price) {
        return webClient
                .put()
                //path variable
                .uri("/{id}", id)
                .body(Mono.just(new Product(null, name, price)), Product.class)
                .retrieve()
                .bodyToMono(Product.class)
                .doOnSuccess(o -> System.out.println("**********UPDATE " + o));
    }

    public Mono<Void> deleteProduct(String id) {
        return webClient
                .delete()
                .uri("/{id}", id)
                .retrieve()
                .bodyToMono(Void.class)
                .doOnSuccess(o -> System.out.println("**********DELETE " + o));
    }

    public Flux<ProductEvent> getAllEvents() {
        return webClient
                .get()
                .uri("/events")
                .retrieve()
                .bodyToFlux(ProductEvent.class);
    }
}
