package com.example.weblux.server.controller;

import com.example.weblux.model.Product;
import com.example.weblux.model.ProductEvent;
import com.example.weblux.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductRepository productRepository;

    @GetMapping
    public Flux<Product> getAllProducts() {
        return productRepository.findAll();
    }

    /**
     * Ritorno Mono perchè è uno e una responseEntity perchè ci sarà un'eccezione in caso di not found
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public Mono<ResponseEntity<Product>> getAllProducts(@PathVariable String id) {
        return productRepository.findById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Product> save(@RequestBody Product p) {
        return productRepository.save(p);
    }

    /**
     * Ricerca il prodotto esistente: se lo trova lo aggiorna altrimenti eccezione
     *  .flatMap() creates a new stream out of every upstream event
     *  (one-to-many mapping, you take one upstream stream, and create a stream from it that can have multiple events)
     */
    @PutMapping("{id}")
    public Mono<ResponseEntity<Product>> update(@PathVariable String id,
                                                @RequestBody Product p) {
        return productRepository.findById(id)
                .flatMap(existing -> {
                    existing.setName(p.getName());
                    existing.setPrice(p.getPrice());
                    return productRepository.save(existing);
                })
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());

    }

    @DeleteMapping("{id}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable String id) {
        return productRepository.findById(id)
                .flatMap(existing ->
                    productRepository.delete(existing)
                            .then(Mono.just(ResponseEntity.ok().<Void>build())))
                .defaultIfEmpty(ResponseEntity.notFound().build());

    }

    @DeleteMapping
    public Mono<Void> deleteAllProducts() {
        return productRepository.deleteAll();
    }


    //Server side events.
    //TEXT_EVENT_STREAM_VALUE indica che stiamo producendo degli eventi di tipo testo
    @GetMapping(value = "/events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ProductEvent> getProductEvents() {
        //Emette valori che iniziano con 0 e li incrementa ogni secondo.
        //Simula uno streaming di eventi
        return Flux.interval(Duration.ofSeconds(1))
                .map(val ->
                        new ProductEvent(val, "Product Event")
                );
    }
}
