package com.example.weblux.server.handler_functions;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.TEXT_EVENT_STREAM;

@Configuration
public class RouteConfig {


    //Associa il path delle api esposte con il metodo che gestisce la request
	@Bean
    RouterFunction<ServerResponse> routes(ProductHandler handler) {
        return RouterFunctions.route()
				.GET("/functional/products/events", RequestPredicates.accept(TEXT_EVENT_STREAM), handler::getProductEvents)
				.GET("/functional/products/{id}", RequestPredicates.accept(APPLICATION_JSON), handler::getProduct)
				.GET("/functional/products", RequestPredicates.accept(APPLICATION_JSON), handler::getAllProducts)
				.PUT("/functional/products/{id}", RequestPredicates.accept(APPLICATION_JSON), handler::updateProduct)
				.POST("/functional/products", RequestPredicates.contentType(APPLICATION_JSON), handler::saveProduct)
				.DELETE("/functional/products/{id}", RequestPredicates.accept(APPLICATION_JSON), handler::deleteProduct)
				.DELETE("/functional/products", RequestPredicates.accept(APPLICATION_JSON), handler::deleteAllProducts)
				.build();

        //Alernativa con nest per mettere a fattor comune gli stessi (per le get infatti non server accept)
        /*return route()
                .path("/products",
                        builder -> builder
                                .nest(accept(APPLICATION_JSON).or(contentType(APPLICATION_JSON)).or(accept(TEXT_EVENT_STREAM)),
                                        nestedBuilder -> nestedBuilder
                                                .GET("/events", handler::getProductEvents)
                                                .GET("/{id}", handler::getProduct)
                                                .GET(handler::getAllProducts)
                                                .PUT("/{id}", handler::updateProduct)
                                                .POST(handler::saveProduct)
                                )
                                .DELETE("/{id}", handler::deleteProduct)
                                .DELETE(handler::deleteAllProducts)
                ).build();*/
    }
}
