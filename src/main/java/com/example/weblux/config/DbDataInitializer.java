package com.example.weblux.config;

import com.example.weblux.model.Product;
import com.example.weblux.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;

@Configuration
public class DbDataInitializer {

    @Bean
    CommandLineRunner init(ProductRepository repository)
    {
        return args -> {
            //Salvo i prodotti su mongo
            Flux<Product> products = Flux.just(
                    new Product(null, "Latte", 2.99),
                    new Product(null, "Caffè", 2.49),
                    new Product(null, "Tea", 1.99))
                    .flatMap(repository::save);

            //Verifico siano salvati
            //L'operatore thanMany permette al primo publisher di completare l'operazione
            products.thenMany(repository.findAll())
                    .subscribe(System.out::println);
        };
    }
}
